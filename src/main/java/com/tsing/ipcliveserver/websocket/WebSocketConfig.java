package com.tsing.ipcliveserver.websocket;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;

/**
 * WebSocket配置类
 *
 * @author TheTsing
 */
@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {

    @Override
    public void registerWebSocketHandlers(WebSocketHandlerRegistry registry) {
        registry.addHandler(messageHandler(), "/websocket/live").setAllowedOriginPatterns("*");
        registry.addHandler(messageHandler(), "/websocket/live_SockJS").setAllowedOriginPatterns("*").withSockJS();
    }

    @Bean
    public WebSocketHandler messageHandler() {
        return new MessageHandler();
    }

}
