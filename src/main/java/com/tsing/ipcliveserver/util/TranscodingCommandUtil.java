package com.tsing.ipcliveserver.util;

import com.tsing.ipcliveserver.exception.BadRequestException;

/**
 * 转码指令生成工具类
 *
 * @author TheTsing
 */
public class TranscodingCommandUtil {

    public static String generateTranscodingCommand(String url) {
        if (url.toLowerCase().startsWith("rtmp")) {
            return generateRtmpTranscodingCommand(url);
        } else if (url.toLowerCase().startsWith("rtsp")) {
            return generateRtspTranscodingCommand(url);
        } else {
            throw new BadRequestException("Please provide a valid video stream url");
        }
    }

    private static String generateRtmpTranscodingCommand(String url) {
        // -loglevel error
        return FileUtil.getTranscoder() + " -loglevel quiet -listen_timeout 10 -re -i " + url + " -r 25 -c:v libx264 -profile:v baseline -b:v 1000k -bufsize 1000k -pix_fmt yuv420p -s 720*480 -tune:v zerolatency -preset:v ultrafast -an -f flv -";
    }

    private static String generateRtspTranscodingCommand(String url) {
        return FileUtil.getTranscoder() + " -loglevel quiet -timeout 3000000 -re -rtsp_transport tcp -i " + url + " -r 25 -c:v libx264 -profile:v baseline -b:v 1000k -bufsize 1000k -pix_fmt yuv420p -s 720*480 -tune:v zerolatency -preset:v ultrafast -an -f flv -";
    }

}
