package com.tsing.ipcliveserver.util;

import com.tsing.ipcliveserver.exception.BadRequestException;
import org.springframework.core.io.ClassPathResource;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

/**
 * 文件工具类
 *
 * @author TheTsing
 */
public class FileUtil {

    public static String getTranscoder() {
        Path target = Paths.get(System.getProperty("java.io.tmpdir"), "IPCLiveServer", "Transcoder.exe");
        if (Files.exists(target)) {
            return target.toString();
        }
        ClassPathResource resource = new ClassPathResource("ffmpeg/ffmpeg.exe");
        try {
            Files.createDirectories(target.getParent());
            Files.copy(resource.getInputStream(), target, StandardCopyOption.REPLACE_EXISTING);
            return target.toString();
        } catch (Exception e) {
            throw new BadRequestException("Failed to create temporary file");
        }
    }

}
