package com.tsing.ipcliveserver.util;

import java.text.DecimalFormat;

/**
 * 数据大小工具类
 *
 * @author TheTsing
 */
public class DataSizeUtil {

    private static final String[] UNIT_NAMES = new String[]{"B", "KB", "MB", "GB", "TB", "PB", "EB"};

    public static String format(long size) {
        if (size <= 0) {
            return "0";
        }
        int digitGroups = Math.min(UNIT_NAMES.length - 1, (int) (Math.log10(size) / Math.log10(1024)));
        return new DecimalFormat("#,##0.##")
                .format(size / Math.pow(1024, digitGroups)) + " " + UNIT_NAMES[digitGroups];
    }

}
