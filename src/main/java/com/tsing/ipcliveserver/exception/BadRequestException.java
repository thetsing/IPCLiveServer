package com.tsing.ipcliveserver.exception;

import org.springframework.http.HttpStatus;

/**
 * 自定义业务异常类
 *
 * @author TheTsing
 */
public class BadRequestException extends RuntimeException {

    private Integer status = HttpStatus.BAD_REQUEST.value();

    public BadRequestException(String message) {
        super(message);
    }

    public BadRequestException(HttpStatus status, String message) {
        super(message);
        this.status = status.value();
    }

    public Integer getStatus() {
        return status;
    }

}
