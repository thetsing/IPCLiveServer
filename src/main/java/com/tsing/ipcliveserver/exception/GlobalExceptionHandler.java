package com.tsing.ipcliveserver.exception;

import com.tsing.ipcliveserver.util.DataSizeUtil;
import org.apache.catalina.connector.ClientAbortException;
import org.apache.tomcat.util.http.fileupload.impl.FileSizeLimitExceededException;
import org.apache.tomcat.util.http.fileupload.impl.SizeLimitExceededException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.MaxUploadSizeExceededException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.stream.Collectors;

/**
 * 全局异常处理器
 * <p>
 * 200	OK                      成功
 * 400  Bad Request             客户端请求存在语法错误或业务异常
 * 401  Unauthorized            认证失败
 * 403  Forbidden               没有权限
 * 404	Not Found               无法找到资源
 * 500	Internal Server Error   服务器内部错误
 * <p>
 *
 * @author TheTsing
 */
@RestControllerAdvice
public class GlobalExceptionHandler {

    private static final Logger log = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    /**
     * 处理所有不可知的异常
     */
    @ExceptionHandler(Throwable.class)
    public ResponseEntity<String> handleThrowable(Throwable e) {
        StringWriter stringWriter = new StringWriter();
        try (PrintWriter printWriter = new PrintWriter(stringWriter)) {
            e.printStackTrace(printWriter);
        }
        log.error(stringWriter.toString());
        return ResponseEntity.internalServerError().body(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());
    }

    /**
     * 处理自定义业务异常
     */
    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<String> handleBadRequestException(BadRequestException e) {
        return ResponseEntity.status(e.getStatus()).body(e.getMessage());
    }

    /**
     * 参数校验异常
     * 关键词：@Validated、@RequestBody、Java bean
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<String> handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        return ResponseEntity.badRequest().body(e.getBindingResult().getAllErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage).collect(Collectors.joining(", ")));
    }

    /**
     * 参数校验异常
     * 关键词：@Validated、@ModelAttribute、Java bean
     */
    @ExceptionHandler(BindException.class)
    public ResponseEntity<String> handleBindException(BindException e) {
        return ResponseEntity.badRequest().body(e.getBindingResult().getAllErrors().stream().map(DefaultMessageSourceResolvable::getDefaultMessage).collect(Collectors.joining(", ")));
    }

    /**
     * 缺少必需的请求参数
     * 关键词：@RequestParam(required = true)
     */
    @ExceptionHandler(MissingServletRequestParameterException.class)
    public ResponseEntity<String> handleMissingServletRequestParameterException(MissingServletRequestParameterException e) {
        return ResponseEntity.badRequest().body(String.format("The required parameter '%s' is missing", e.getParameterName()));
    }

    /**
     * 缺少必需的Part
     * 关键词：@RequestPart(required = true)
     */
    @ExceptionHandler(MissingServletRequestPartException.class)
    public ResponseEntity<String> handleMissingServletRequestPartException(MissingServletRequestPartException e) {
        return ResponseEntity.badRequest().body(String.format("The required request part '%s' is missing", e.getRequestPartName()));
    }

    /**
     * 参数类型不匹配
     * 关键词：@RequestParam
     */
    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<String> handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException e) {
        return ResponseEntity.badRequest().body(String.format("The parameter '%s' with value '%s' could not be converted to type '%s'", e.getName(), e.getValue(), e.getRequiredType().getSimpleName()));
    }

    /**
     * 请求参数读取解析异常
     */
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<String> handleHttpMessageNotReadableException() {
        return ResponseEntity.badRequest().body("The request parameter could not be parsed");
    }

    /**
     * 超出最大上传大小异常
     */
    @ExceptionHandler(MaxUploadSizeExceededException.class)
    public ResponseEntity<String> handleMaxUploadSizeExceededException(MaxUploadSizeExceededException e) {
        String msg;
        if (e.getCause().getCause() instanceof FileSizeLimitExceededException fileSizeLimitExceededException) {
            msg = String.format("The file size exceeds the maximum allowed size '%s'", DataSizeUtil.format(fileSizeLimitExceededException.getPermittedSize()));
        } else if (e.getCause().getCause() instanceof SizeLimitExceededException sizeLimitExceededException) {
            msg = String.format("The request size '%s' exceeds the maximum allowed size '%s'", DataSizeUtil.format(sizeLimitExceededException.getActualSize()), DataSizeUtil.format(sizeLimitExceededException.getPermittedSize()));
        } else {
            msg = "The file size exceeds the maximum allowed size";
        }
        return ResponseEntity.badRequest().body(msg);
    }

    /**
     * 内容类型不支持
     */
    @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
    public ResponseEntity<String> handleHttpMediaTypeNotSupportedException(HttpMediaTypeNotSupportedException e) {
        return ResponseEntity.badRequest().body(String.format("The media type '%s' is not supported. Supported media types are %s", e.getContentType(), e.getSupportedMediaTypes()));
    }

    /**
     * 请求方法不支持
     */
    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    public ResponseEntity<String> handleHttpRequestMethodNotSupportedException(HttpRequestMethodNotSupportedException e) {
        return ResponseEntity.badRequest().body(String.format("The requested method '%s' is not supported. Supported methods are %s", e.getMethod(), e.getSupportedHttpMethods()));
    }

    /**
     * 客户端提前关闭连接
     */
    @ExceptionHandler(ClientAbortException.class)
    public void handleClientAbortException() {
    }

}
