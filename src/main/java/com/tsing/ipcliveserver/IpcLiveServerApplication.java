package com.tsing.ipcliveserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IpcLiveServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(IpcLiveServerApplication.class, args);
    }

}
